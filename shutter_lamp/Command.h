#pragma once

class Command {
    /*
      Implements a composite serial-command pattern:
      you can add commands as objects (separate commands).
      and serves as base-class for that.
      One command starts your object, you'd have to implement sub-commands.
      '?' is help
      'X' calls teardown on the current command (stops calling it's loop)

      When there is input on Serial, find the object that will handle that character.

      Usage:
        class YourClass : Command { implement description, setup, loop, & teardown }

      Lifecycle:
        // on new command:
          previous.teardown()
          new.setup()
        // every Command::handle()
        new.loop() || new.teardown() // i.e. .loop returns "wants to continue"

      globals:
          #include "Command.h"
          // Each object is subclass of Command
          // Type '?' to get command list
          const Command* Commands[] = {
            // auto assigned a letter:
            new PlotSlider(),  // command 'a'
            new ShowSliderClass(), // command 'b'
            new ShowSwitches()  // command 'c'
            // implicit static handler here, 'X' and '?'
          };
      yourmain.ino
          void loop()
            // see if input, and if any of the handlers recognize it:
            while ( Command::handle(Commands, array_size(Commands)) ) {}
            ...


    */
  public:
    // Implement these
    virtual __FlashStringHelper const * description(); // String suitable for the "help" list
    virtual void setup() {}
    virtual boolean loop() = 0; // return false when loop is done
    virtual void teardown() {}

    static boolean handle(Command * const commands[], unsigned int command_ct) {
      static Command *current = NULL;

      if (Serial.available() > 0) {
        // see if it is a command
        char cmd = Serial.read();
        Serial << F("command ") << cmd << endl;
        Command *new_command = NULL;

        for (unsigned int i = 0; i < command_ct; i++) {
          // eventually, allow commands to have their own char
          if (  cmd >= 'a' && (unsigned int) (cmd - 'a') == i ) {
            new_command = commands[i];
            Serial << F("matched ") << i << endl;
            break;
          }
        }
        if (new_command) {
          if (current) current->teardown();
          current = new_command;
          Serial << F("new current setup") << endl;
          current->setup();

        }
        else {
          switch (cmd) {
            case 'X' :
              if (current) current->teardown();
              current = NULL;
              return false;

            case '?' :
              {
                for (unsigned int i = 0; i < command_ct; i++) {
                  Serial << (char)('a' + i) << F(" : ") << commands[i]->description() << endl;
                }
                Serial << F("X : eXit to normal loop") << endl;
                Serial << F("? ");
                while (Serial.available() < 1) {
                  delay(10);
                }
                return true;
              }
              break;

            case '\010':
            case '\013':
              // ignore EOL
              return false;

            default :
              {
                Serial << F("Wat? ") << cmd << endl;
              }
          }
        }
      }

      boolean doing_command = false;
      if (current) {
        doing_command = current->loop();
        if (! doing_command) {
          current->teardown();
          current = NULL;
        }
      }
      return doing_command;
    }
};

class PlotSlider : public Command {
  public:
    const __FlashStringHelper* description() {
      return F( "Output slider for plotting " "PlotSlider" );
    }
    boolean loop() {
      static float release_slope = -25.0;
      static float end_release = 2.0;

      static boolean released = false;

      static Every period(200);
      static float last_v = 0;
      float v = SmoothedSlider.value();
      if (v < 30) v = 0;

      float slope = v - last_v;

      if ( slope <= release_slope) {
        released = true;
      }
      else if ( released && slope >= end_release) {
        released = false;
      }

      if (period() || (v > 40 && last_v != v)) {

        float slope = v - last_v;
        last_v = v;
        Serial << v << F(" ") << slope * 10 << F(" ") << (released ? -1 : 100) << endl;
      }
      return true;
    }
};

class ShowSliderClass : public Command {
  public:
    const __FlashStringHelper* description() {
      return F("Slider w/logic for release for plotting " "ShowSliderClass");
    }

    void setup() {
      Serial << F("Smoothed: Slope: PressOn: ReleaseSlope: EndRelease: Released:") << endl;
    }

    boolean loop() {
      static Every period(200);

      static int last_v = 0;
      last_v = Brightness.last_raw;
      int v = Brightness.value();
      int slope = Brightness.last_raw - last_v;

      if (period() || (v > 0 && last_v != Brightness.last_raw)) {

        Serial
            << v
            << F(" ") << Brightness.last_raw
            << F(" ") << slope * 10
            << F(" ") << Brightness.max_slope_to_track_press * 10
            << F(" ") << Brightness.slope_at_release * 10
            << F(" ") << Brightness.min_slope_to_end_release * 10
            << F(" ") << (Brightness.released ? -1 : 100)
            << endl;
      }

      return true;
    }
};

class ShowSwitches : public Command {
  public:
    const __FlashStringHelper* description() {
      return F("Switch states for plotting " "ShowSwitches");
    }

    void setup() {
      Serial << F("Master:");
      for (int i = 0; i < BankCt; i++) {
        Serial << F(" Bank[") << i << F("]:");
      }
      Serial << endl;
      Serial << F("  each switch is prefixed by n, 3==on, 0=off, so 23 or 10 etc") << endl;
    }

    boolean loop() {
      static Every period(300);
      int plot_base = -1;

      if (period()) {
        plot_base++;
        Serial << ( MasterPower.value() ? ((plot_base * 10) + 3) : plot_base * 10 );

        for (int i = 0; i < BankCt; i++) {
          plot_base++;
          Serial << F(" ") << (Banks[i].a_switch.value() ? ((plot_base) * 10 + 3) : plot_base * 10 );
        }

        Serial << endl;
      }

      return true;
    }
};

class ManualLED : public Command {
    // manually (<<Serial) control the LEDs

  public:
    const __FlashStringHelper* description() {
      return F("Manual control " "ManualLED");
    }

    void setup() {
      Serial << F("Manually control leds") << endl;
    }

    boolean loop() {
      // we do not expect to keep getting called, we take over the loop

      int bank_i = 0;
      boolean is_green = true;
      while (true) { // till some-other serial command
        boolean update = true;

        if (update) {
          for (int i = 0; i < BankCt; i++) {
            Serial << F("[") << i << F("/") << (char)('a' + i) << F("] ")
                   << ( Banks[i].a_switch.value() ? F("on  ") : F("off ") )
                   << F("G ") << Banks[i].green.value() << F(" ")
                   << F("B ") << Banks[i].blue.value() << F(" ")
                   << endl ;
          }
          update = false;
        }

        Serial << F("Setting bank ") << bank_i
               << F(" ") << ( is_green ? F("green") : F("blue") )
               << endl;
        Serial << F("  z = all off, w = all on 128, + = all on B|G" ) << endl;
        Serial << F("  a|b|c-bank + Green|Blue + 0..9brightness (or any other to exit): ");
        while (Serial.available() <= 0) {
          delay(10);
        }
        char cmd = Serial.read();
        Serial << cmd << endl;

        if (cmd >= 'a' && cmd <= ('a' + BankCt) ) {
          bank_i = cmd - 'a';
        }
        else if (cmd == 'G') {
          is_green = true;
          update = true;
        }
        else if (cmd == 'B' ) {
          is_green = false;
          update = true;
        }
        else if ( cmd >= '0' && cmd <= '9') {
          int value = map( cmd - '0', 0, 9, 0, 255 );
          if (is_green) {
            Banks[ bank_i ].green = value;
          }
          else {
            Banks[ bank_i ].blue = value;
          }
        }
        else if (cmd == 'z' ) {
          for (int i = 0; i < BankCt; i++) {
            Banks[i].green = 0;
            Banks[i].blue = 0;
          }
        }
        else if (cmd == 'w' ) {
          for (int i = 0; i < BankCt; i++) {
            Banks[i].green = 128;
            Banks[i].blue = 128;
          }
        }
        else if (cmd == '+' ) {
          for (int i = 0; i < BankCt; i++) {
            if (is_green) {
              Banks[ i ].green = 128;
            }
            else {
              Banks[ i ].blue = 128;
            }
          }
        }
        else {
          break;
        }
      }

      return false;
    }
};

class SerialTest : public Command {
    // various serial tests

  public:
    struct State {
      boolean dtr;
      boolean udfnuml;
      boolean serial;
    };
    char buffer[ 10 ];
    int buffer_i = 0;

    const __FlashStringHelper* description() {
      return F("Serial Tests " "SerialTest");
    }

    void setup() {
      Serial << F("Change switch [0] to exit") << endl;
      for (unsigned int i = 0; i < array_size(buffer); i++) {
        buffer[i] = '-';
      }
    }

    boolean loop() {

      State current = { false, false, false};
      State was_state = current;

      note_state(current);

      while ( Banks[0].a_switch.value() ) {
        boolean changed = false;
        current.dtr = Serial.dtr();
        changed = changed || make_note(current.dtr != was_state.dtr, 'd');
        current.udfnuml = UDFNUML;
        changed = changed || make_note(current.udfnuml != was_state.udfnuml, 'U');
        current.serial = !!Serial;
        changed = changed || make_note(current.serial != was_state.serial, 'S');

        if (changed) {
          was_state = current;
          note_state(current);

          Banks[2].green = 0;
          delay(100);
          Banks[2].green = 128;
          delay(100);
          Banks[2].green = 0;
        }
      }

      Serial << F("Switch 0 went to off, exit") << endl;
      return false;
    }

    boolean make_note(boolean changed, char log_char) {
      if (changed) {
        buffer[buffer_i] = log_char;
        buffer_i = (buffer_i + 1) % array_size(buffer);
      }
      return changed;
    }

    void note_state(State &s) {
      Serial << buffer_i << F(": ");
      for (unsigned int i = 0; i < array_size(buffer); i++) {
        Serial << (char) buffer[ (i + buffer_i) % array_size(buffer) ];
      }
      Serial << endl;

      Serial << F("Change to:")
             << F(" dtr ") << s.dtr
             << F(" UDFNUML ") << s.udfnuml
             << F(" !!Serial ") << s.serial
             << endl;
    }
};
