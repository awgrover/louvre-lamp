#pragma once

// #include <awg_combinators/value_iface.h>

//template <typename T> // anything with .value(), but intended for an analog-read derivative: float
class SliderPot : public ValueSource {
    /* logic to handle reading the slider-pot:
      Picking up a finger causes a negative >> slope, don't track it down.
      Touching causes a positive slope (takes a bit), don't track till small slope (~4).
      Sliding causes fairly small slopes, track.
    */

  public:
    // remember, same order as initializer
    ValueSource * const valuable;
    int current; // current value, use () or .value() to update
    const int min_value;
    const int slope_at_release;
    const int min_slope_to_end_release;
    const int max_slope_to_track_press;

    boolean released = true; // pretend we have a value at startup
    int last_raw = current;
    boolean release_done = true; // detect end of release slope
    
    SliderPot(
      ValueSource *valuable, // somebody with .value(), i.e. ExponentialSmoother(new AnalogPinWithDelay(A0), 5);
      int initial_brightness,
      int min_value,  // ignore below this value
      // "slope" is really just delta from last value
      int slope_at_release, // a negative slope that indicates release, more << than manual slide
      int min_slope_to_end_release, // a positive value above the noise that means real input (touch)
      int max_slope_to_track_press // when slope <<, then start tracking press
    )
      : valuable(valuable),
        current (initial_brightness),
        min_value(min_value),
        slope_at_release(slope_at_release),
        min_slope_to_end_release(min_slope_to_end_release),
        max_slope_to_track_press(max_slope_to_track_press)
    {
    }

    int raw_value() {
      return valuable->value();
    }

    inline int operator()() {
      return value();
    }

    boolean detect_released(int slope) {

      if ( slope <= slope_at_release) {
        release_done = false;
        released = true;
      }
      else if ( released && slope >= min_slope_to_end_release) {
        released = false;
      }
      return released;
    }

    boolean detect_press_start(int new_value, int slope) {
      if (released) {
        if ( new_value > 0 && release_done && slope < max_slope_to_track_press ) {
          released = false;
          return true; // start tracking
        }
        else if ( new_value > current) {
          released = false;
          return true; // start tracking
        }
      }
      return false;
    }

    int value() {
      // min
      // fixme: do a "min" combinator
      int new_value = valuable->value();
      if (new_value < min_value) new_value = 0;

      int slope = new_value - last_raw;

      if (new_value == 0) release_done = true;

      if (released) {
        if ( detect_press_start(new_value, slope) ) current = new_value;
      }
      else if ( detect_released(slope) ) {} // we have last value before release
      else if (new_value != 0) {
        current = new_value; // update
      }

      last_raw = new_value; // always to track slope

      return current;
    }

};
