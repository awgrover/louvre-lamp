/*
  The slider is not reliable. Try tapping at both ends, running to both ends.
  Will work w/o USB, but has to reset w/o USB in that case.
 
  Brightness control
  blue/Green balance
  Bank off/on

  itsy bitys 32u4 3v
    4 pwm
    2k ram
  3v power supply
  touch slider pot
  led filaments, 3v, 120mA
    driver mosfets
    check V (5v?)
*/

#define DEBUGRELEASE 0

#include <awg_combinators.h>
#include <array_size.h>
#include <assert.h>
#include "Slider.h"

// static_assert( A10 == 10, "Itsy bitsy is supposed to be true");

// pin pair for a jumper: jumper on == inhibit serial out, i.e. no usb
DigitalPin _EnableSerialPin(A1, OUTPUT); // we set this to 0
Switch EnableSerialPin(A2);

constexpr int ZeroBrightness = 30; // where "off" really is. experimental derived

ExponentialSmoother SmoothedSlider = ExponentialSmoother(new AnalogPinWithDelay(A0), 5);
SliderPot Brightness = SliderPot(
                         &SmoothedSlider,
                         200, // initial_brightness,
                         30,  // ignore below this value, i.e. off
                         // "slope" is really just delta from last value
                         -20, // slope_at_release, // a negative slope that indicates release, more << than manual slide
                         2, // min_slope_to_end_release, // a positive value above the noise that means real input (touch)
                         4 // max_slope_to_track_press // when slope <<, then start tracking pres
                       );
// clip ends of possible pot
constexpr int MinBrightness = 1;
constexpr int MaxBrightness = 1020;

Debounce MasterPower = Debounce(new Switch(8));

// Banks of lights
struct BankType {
  PWMPin green;
  PWMPin blue;
  Debounce a_switch;
};

// don't use: 0,1,5
// pwm: 3,6,9,10,11,13(intled)
// 9=A9, 10=A10,12=A11, 6=A7
BankType Banks[] = {
  {
    PWMPin(11),
    PWMPin(13), // 13 is a pwm builtin,
    Debounce(new Switch(12))
  },
  {
    PWMPin(9),
    PWMPin(10),
    Debounce(new Switch(7))
  },
  {
    PWMPin(3),
    PWMPin(6),
    Debounce(new Switch(4))
  }
};

constexpr int BankCt = array_size(Banks);

ExponentialSmoother Balance = ExponentialSmoother(new AnalogPinWithDelay(A1), 10);
// ignore ends
constexpr int MinBalance = 3;
constexpr int MaxBalance = 1020;

// last, so we get access to globals
#include "Command.h"
// Type '?' to get command list
Command * const Commands[] = {
  new PlotSlider(),
  new ShowSliderClass(),
  new ShowSwitches(),
  new ManualLED(),
  new SerialTest(),
};

boolean do_debug = false;

void setup() {
  static Timer static_timeout(5000);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200); while (!static_timeout() && !Serial) {
    delay(300); digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }
  while (! static_timeout.after() ) {
    static_timeout();
    delay(300); digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }

  Serial.println(F( "Start " __FILE__ " " __DATE__ " " __TIME__ " gcc " __VERSION__));
  if (DEBUGRELEASE) {
    Serial << "DEBUGRELEASE" << endl;
  }

  _EnableSerialPin.setup();
  EnableSerialPin.setup();

  _EnableSerialPin = LOW;
  Serial << F("Enable Serial-in (A1--A2 jumper): ") << ( !EnableSerialPin.value() ? "Yes" : "No" ) << endl;

}

Timer DebugOutput(200);

void loop() {
  if ( ! EnableSerialPin.value() ) {
    while ( Command::handle(Commands, array_size(Commands)) ) {}
  }
  do_debug = DebugOutput.after();

  int smoothed_balance = Balance.value();
  int constrained_balance = constrain(smoothed_balance, MinBalance, MaxBalance);
  int balance = map(constrained_balance, MinBalance, MaxBalance, (int)0, (int)255);

  float brightness = get_brightness();

  int green = balance;
  int blue = 256 - green;

  if (do_debug) {
    Serial
        << F(" s~") << smoothed_balance
        << F(" <>~") << constrained_balance
        << F(" ~") << balance
        << F(" !!") << brightness
        << F(" x") << int(brightness * 255)
        << F(" g") << green << F(" b") << blue << F(" ")
        << endl;
  }

  adjust_to_brightness( brightness, green, blue );
  if (do_debug) {
    Serial
        << green << F(" ") << blue << F(" ");
  }

  if (do_debug) Serial << " [i] G/B ";
  set_bank_leds( Banks, green, blue, BankCt );

  if (do_debug) {
    Serial << endl;
    DebugOutput.reset();
  }
}

void set_bank_leds( BankType bank[], int green, int blue, int ct ) {
  int power = MasterPower.value();

  if (do_debug) {
    Serial
        << F(" 1/0 ") << power << F(" ");
  }

  for (int i = 0; i < ct; i++) {
    if (do_debug) {
      Serial
          << F(" [") << i << F("] ");
    }
    if ( power && bank[i].a_switch.value() ) {
      bank[i].green = green;
      bank[i].blue = blue;
      if (do_debug) {
        Serial
            << green << F("/") << blue << F(" ")
            ;
      }
    }
    else {
      bank[i].green = 0;
      bank[i].blue = 0;
      if (do_debug)  Serial << F("OFF ") ;
    }

  }
}

void adjust_to_brightness(float brightness, int &green, int &blue) {
  const float balanced_range = 1.5;

  // "balanced" brightness for up to this part of the range
  if ( brightness <= balanced_range ) {
    float b_amp = brightness / balanced_range;
    green = round(green * b_amp);
    blue = round(blue * b_amp);
  }
  else {
    // 1.0 .. 2.0, just "add" amp
    int lowest = min(green, blue);
    int remaining = 255 - lowest; // max "amp" possible

    // amount to add
    int amp = map(brightness, balanced_range, 2.0, 1, remaining);

    // add an clip
    green = max(255, green + amp);
    blue = max(255, blue + amp);
  }
}

float get_brightness() {
  // 0.0 .. 1.0 is "normal" brightness (*).
  // 1.0 .. 2.0 is "overdrive" (+)
  // Usually don't get full max value, so treat max as MaxBrightness (clip)
  // Need to distinguish: a touch at "off" from finger-lift:
  // Usually don't get 0 as min, so: MinPotValue as "0"
  // One "finger" width of range above that will be "off"
  // read value, if >> 0, then it is new value
  // if it is 0 and last >> 0, then no change
  // if 0 and last ~ 0, then 0

  static float last_value = 1.0 / 4; // FIXME: save in flash

  // has logic for release & press & track:
  float raw_brightness = Brightness.value();

  float constrained_brightness = constrain(raw_brightness, MinBrightness, MaxBrightness);

  if (do_debug) {
    Serial << F(" r") << raw_brightness
           << F(" <>") << constrained_brightness;
  }

  float mapped_brightness = -1;

  if (raw_brightness >= ZeroBrightness ) {
    mapped_brightness = map((float) constrained_brightness, (float) MinBrightness, (float) MaxBrightness, 0.0, 2.0);
    if (do_debug && last_value != mapped_brightness) {
      Serial << F(" m") << mapped_brightness
             << F(" [") << last_value << F("] ")
             << endl
             ;
    }
    last_value = mapped_brightness;
  }
  if (do_debug) Serial << endl;

  //do_debug = raw_brightness != 0;
  return last_value;
}
